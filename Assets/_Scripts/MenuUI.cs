using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.Localization.Settings;
using UnityEngine.Localization;
using static UnityEditor.PlayerSettings.Switch;

public class MenuUI : MonoBehaviour
{
    public enum Keybinds { A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, 
        Space, Tab, LeftShift, RightShift, UpArrow, DownArrow, LeftArrow, RightArrow }

    public Manager manager;

    private Button playButton;
    private Button settingsButton;
    private Button quitButton;
    private Button closeSettingsButton;

    private DropdownField languageDropdown;

    private EnumField jumpKeyEnum;

    private VisualElement settingsParent;
    private VisualElement settingsUnderline;
    private VisualElement mainMenu;

    private Tween settingsUnderlineTween;

    private bool gameMode = false;

    void Start()
    {
        gameMode = false;

        VisualElement root = GetComponent<UIDocument>().rootVisualElement;

        playButton = root.Q<Button>("PlayButton");
        settingsButton = root.Q<Button>("SettingsButton");
        quitButton = root.Q<Button>("QuitButton");
        closeSettingsButton = root.Q<Button>("CloseSettingsButton");
        languageDropdown = root.Q<DropdownField>("LanguageDropdown");
        jumpKeyEnum = root.Q<EnumField>("JumpKey");
        settingsParent = root.Q<VisualElement>("SettingsParent");
        settingsUnderline = root.Q<VisualElement>("SettingsUnderline");
        mainMenu = root.Q<VisualElement>("ButtonContainer");


        playButton.RegisterCallback<ClickEvent>(OnPlayButtonClicked);
        settingsButton.RegisterCallback<ClickEvent>(OnSettingsButtonClicked);
        quitButton.RegisterCallback<ClickEvent>(OnQuitButtonClicked);
        closeSettingsButton.RegisterCallback<ClickEvent>(OnCloseSettingsButtonClicked);
        languageDropdown.RegisterCallback<ChangeEvent<string>>(OnLanguageChanged);
        jumpKeyEnum.RegisterCallback<ChangeEvent<Enum>>(OnJumpKeyChanged);

        settingsParent.AddToClassList("SettingsUp");

        List<string> languages = new List<string>();

        for (int i = 0; i < LocalizationSettings.AvailableLocales.Locales.Count; i++)
        {
            Locale locale = LocalizationSettings.AvailableLocales.Locales[i];
            languages.Add(locale.LocaleName);

            if (LocalizationSettings.SelectedLocale == locale)
            {
                languageDropdown.value = locale.LocaleName;
            }
        }
        
        languageDropdown.choices = languages;

        if (PlayerPrefs.HasKey("JumpKey"))
        {
            jumpKeyEnum.value = (Keybinds)Enum.Parse(typeof(Keybinds), PlayerPrefs.GetString("JumpKey"));
        }
        else
            jumpKeyEnum.value = Keybinds.Z;
    }

    private void OnJumpKeyChanged(ChangeEvent<Enum> change)
    {
        Player.SET_JUMP_KEY ((KeyCode)Enum.Parse(typeof(KeyCode), change.newValue.ToString()));
    }

    private void OnCloseSettingsButtonClicked(ClickEvent clickEvent)
    {
        settingsParent.AddToClassList("SettingsUp");
        settingsUnderlineTween.Kill();

        if(gameMode)
            manager.ManualUnpause();
    }

    private void OnPlayButtonClicked(ClickEvent clickEvent)
    {
        Debug.Log("Playing the game");
    }
    private void OnSettingsButtonClicked(ClickEvent clickEvent)
    {
        settingsParent.RemoveFromClassList("SettingsUp");

        float startPos = -settingsUnderline.localBound.width * 0.2f;
        float endPos = settingsUnderline.localBound.width * 0.2f;
        float pos = startPos;

        settingsUnderlineTween = DOTween.To(() => startPos, x => pos = x, endPos, 1.5f)
            .OnUpdate(() =>
            {
                settingsUnderline.transform.position = new Vector3(pos, 0);
            })
            .SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine).SetUpdate(true);

    }
    private void OnQuitButtonClicked(ClickEvent clickEvent)
    {
        Debug.Log("Quiting the game");
    }

    private void OnLanguageChanged(ChangeEvent<string> change)
    {
        Debug.Log($"Changing to {change.newValue}");
        for (int i = 0; i < LocalizationSettings.AvailableLocales.Locales.Count; i++)
        {
            Locale locale = LocalizationSettings.AvailableLocales.Locales[i];

            if (change.newValue == locale.LocaleName)
            {
                LocalizationSettings.SelectedLocale = locale;
            }
        }
    }

    public void Pause()
    {
        settingsParent.RemoveFromClassList("SettingsUp");

        float startPos = -settingsUnderline.localBound.width * 0.2f;
        float endPos = settingsUnderline.localBound.width * 0.2f;
        float pos = startPos;

        settingsUnderlineTween = DOTween.To(() => startPos, x => pos = x, endPos, 1.5f)
            .OnUpdate(() =>
            {
                settingsUnderline.transform.position = new Vector3(pos, 0);
            })
            .SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine).SetUpdate(true);
    }

    public void Unpause()
    {
        settingsParent.AddToClassList("SettingsUp");
        settingsUnderlineTween.Kill();
    }

    public void HideMainMenu()
    {
        mainMenu.AddToClassList("menuUp");
        gameMode = true;
    }
}
