using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    public MenuUI menuUI;

    private bool isPaused;

    private KeyCode pause = KeyCode.Escape;

    private void Start()
    {
        isPaused = false;
        menuUI.HideMainMenu();
    }

    private void Update()
    {
        if(Input.GetKeyDown(pause))
        {
            if (isPaused)
            {
                Time.timeScale = 1.0f;
                isPaused = false;
                menuUI.Unpause();
            }
            else
            {
                Time.timeScale = 0f;
                isPaused = true;
                menuUI.Pause();
            }
        }
    }

    public void ManualUnpause()
    {
        Time.timeScale = 1.0f;
        isPaused = false;
    }
}
