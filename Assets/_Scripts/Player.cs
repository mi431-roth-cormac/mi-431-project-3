using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public enum PlayerPowerup { Red, Orange, Yellow, Green, Blue, Violet };

    public static Player S;

    private KeyCode jumpKey = KeyCode.Z;

    private Rigidbody2D rb;
    private int jumpThisFrame = 0;
    private int jumpBuffer = 5;
    private int coyoteBuffer = 5;
    private int framesSinceGrounded = -10;
    private float floorDistance = 0.25f;
    private bool grounded;

    [Header("Player Movement Settings")]
    public float topSpeed = 10f;
    public float acceleration = 0.5f;
    public float decceleration = 0.5f;
    public float jumpSpeed = 10f;
    public float downPullMult = 1f;
    public LayerMask groundCheckMask;

    [Header("Dust Animations")]
    public ParticleSystem jumpParticles;
    public ParticleSystem landParticles;
    public ParticleSystem trailParticles;

    [Header("Power-ups")]
    [SerializeField]
    private PlayerPowerup powerup = PlayerPowerup.Violet;
    public SpriteRenderer sRend;
    private Color hexColor;

    private void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        S = this;
        hexColor = sRend.color;
    }

    private void Start()
    {
        powerup = PlayerPowerup.Violet;
        S.SetColor();
        trailParticles.Play();

        if (PlayerPrefs.HasKey("JumpKey"))
        {
            jumpKey = (KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("JumpKey"));
        }
    }

    private void Update()
    {
        if(Input.GetKeyDown(jumpKey)) 
        {
            jumpThisFrame = jumpBuffer;
        }
    }

    private void FixedUpdate()
    {
        // Check if we are grounded and get current input
        GroundCheck();
        float dir = Input.GetAxisRaw("Horizontal");
        float currDir = rb.velocity.x / Mathf.Abs(rb.velocity.x);

        // Bring near zero x velocities to zero
        if (Mathf.Abs(dir) <= 0.005 && Mathf.Abs(rb.velocity.x) < acceleration)
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
        }
        else
        {
            // Accelerate in the current direction if below top speeds
            rb.velocity += new Vector2(dir * acceleration, 0);

            if (Mathf.Abs(rb.velocity.x) > topSpeed)
                rb.velocity = new Vector2(currDir * topSpeed, rb.velocity.y);

            // If the character is moving and the player is not pressing that key,
            // deccelerate from moving that direction
            if (Mathf.Abs(currDir - dir) >= 0.005 && rb.velocity.x != 0)
            {
                rb.velocity -= new Vector2(currDir * acceleration, 0);
            }

            // Create particle effects when changing directions quickly
            if (Mathf.Abs(currDir - dir) >= 0.005 && Mathf.Abs(dir) >= 0.005)
            {
                if (grounded)
                    CreateLandParticles();
                else
                    CreateJumpParticles();
            }
        }

        // Handle jump requests: jump when able, and reduce the counter on frames
        // since last jump request.
        // Additionally, apply additional gravity when the character is falling
        if(jumpThisFrame > 0 && (grounded || framesSinceGrounded <= coyoteBuffer))
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpSpeed);
            CreateJumpParticles();
            jumpThisFrame = 0;
        }
        else if (!grounded)
        {
            rb.velocity += Physics2D.gravity * downPullMult * Time.deltaTime;
            --jumpThisFrame;
            framesSinceGrounded++;
        }
    }

    private void GroundCheck()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up, floorDistance, groundCheckMask);

        if (hit.collider != null)
        {
            if (!grounded)
            {
                trailParticles.Play();
                CreateLandParticles();
            }
            grounded = true;
            framesSinceGrounded = 0;
        }
        else
        { 
            if (grounded)
                trailParticles.Pause();
            grounded = false; 
        }
    }

    void CreateJumpParticles()
    {
        ParticleSystem.MainModule main = jumpParticles.main;
        main.startColor = hexColor;
        jumpParticles.Play();
    }

    void CreateLandParticles()
    {
        landParticles.Play();
    }

    void SetColor()
    {
        switch(powerup)
        {
            case PlayerPowerup.Red:
                hexColor = new Color(0.5294118f, 0.1058824f, 0.1534774f);
                break;
            case PlayerPowerup.Orange:
                hexColor = new Color(0.5283019f, 0.2891502f, 0.1071556f);
                break;
            case PlayerPowerup.Yellow:
                hexColor = new Color(0.7186316f, 0.764151f, 0.3280082f);
                break;
            case PlayerPowerup.Green:
                hexColor = new Color(0.03921568f, 0.2666667f, 0.04302263f);
                break;
            case PlayerPowerup.Blue:
                hexColor = new Color(0.03921568f, 0.1164032f, 0.2666667f);
                break;
            case PlayerPowerup.Violet:
                hexColor = new Color(0.2164005f, 0.03993588f, 0.2679245f);
                break;
        }
        sRend.color = hexColor;

        ParticleSystem.MainModule main = trailParticles.main;
        main.startColor = hexColor;
    }

    public static void SET_POWER(PlayerPowerup power)
    {
        S.powerup = power;
        S.SetColor();
    }

    public static void SET_JUMP_KEY(KeyCode key)
    {
        S.jumpKey = key;
        PlayerPrefs.SetString("JumpKey", key.ToString());
    }
}
